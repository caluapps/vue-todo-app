   const LOCAL_STORAGE_KEY = 'todo-app-vue';
   new Vue({
      el: '#app',
      data: {
         title: 'Todos!',
         todos: JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY)) || [{
            text: 'Learn JavaScript',
            isDone: true
         }, {
            text: 'Learn Vue',
            isDone: false
         }, {
            text: 'Build something awesome',
            isDone: true
         }],
         editing: null
      },
      methods: {
         createTodo: function(event) {
            const textbox = event.target;
            this.todos.push({
               text: textbox.value.trim(),
               isDone: false
            });
            textbox.value = '';

         },
         startEditing: function(todo) {
            this.editing = todo;

         },
         finishEditing: function(event) {
            if (!this.editing) {
               return;
            }
            const textbox = event.target;
            this.editing.text = textbox.value.trim();
            this.editing = null;

         },
         cancelEditing: function() {
            this.editing = null;

         },
         destroyTodo: function(todo) {
            const index = this.todos.indexOf(todo);
            this.todos.splice(index, 1);

         },
         clearCompleted: function() {
            // console.log('cleared');
            this.todos = this.activeTodos;
         }
      },
      watch: {
         todos: {
            deep: true,
            handler(newValue) {
               localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(newValue));
            }
         }
      },
      computed: {
         activeTodos() {
            return this.todos.filter(t => !t.isDone);
         },
         completedTodos() {
            return this.todos.filter(t => t.isDone);
         }
      }
   });

$(document).ready(function() {
   console.log('vue-todo-app');

});